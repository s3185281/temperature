package nl.utwente.di.bookQuote;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions ;

public class TestQuoter {

    @Test
    public void testBook1() throws Exception{
        Quoter quoter = new Quoter();
        double tempConv = quoter.tempConv(10.0);
        Assertions.assertEquals(338.0, tempConv);
    }
}
